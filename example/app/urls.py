from django.urls import path

from example.app.views import IndexPage

urlpatterns = [
    path('', IndexPage.as_view(), name="index")
]
